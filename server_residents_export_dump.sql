#

connect alfamasdb;

drop table if exists residents_dump;

create table residents_dump
as
select id as id
, cardnumber as cardnumber
, active as active
, concat_ws('-',p1,p2,p3,p4,p5,p6,p7,p8,p9,p10) as privileges
from cards
;
