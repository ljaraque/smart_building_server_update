Server Side Time Attendance Calculation
---

####Crontab must inlcude:  

```
*/5 * * * * root /usr/bin/python /home/naifo/cronjobs/smart_building_server_update/server_to_csv.py >> /home/naifo/cronjobs/smart_building_server_update/crontab_srv_to_csv.log && /usr/bin/python /home/naifo/cronjobs/smart_building_server_update/csv_to_db.py >> /home/naifo/cronjobs/smart_building_server_update/crontab_csv_to_db.log && /usr/bin/python /home/naifo/cronjobs/smart_building_server_update/populate_attendance.py >> /home/naifo/cronjobs/smart_building_server_update/crontab_populate_attend.log && /usr/bin/python /home/naifo/cronjobs/smart_building_server_update/sum_monthly_time.py >> /home/naifo/cronjobs/smart_building_server_update/crontab_sum_monthly_time.log
```

###Creation of data directories
The following directories must be created:  
`data_server/`  
`data_pi/`  
`data_to_pi_access/`  
Their ownerships and persmissions are mentioned in following sections.  

###Initial conditions file  
Copy the file `remaining_events_i-1.csv.initial` with the name `remaining_events_i-1.csv`   

####To allow .py to work properly 
Reference: http://stackoverflow.com/questions/2783313/how-can-i-get-around-mysql-errcode-13-with-select-into-outfile  

**server_to_csv.py:** 

Change ownership of directory data_server to `mysql:mysql` (Under study if this is strictly necessary).  

The following is necessary to let mysql write csv files in `data_to_pi_access` directory:  
```
sudo chown -R mysql:mysql data_to_pi_access/
```
 
Then, to allow mysql to write into files:  
**Note:** this steps seems to depend of Ubuntu version. Check version with `lsb_release -a`.  
In our test case in ubuntu Ubuntu 12.04.5 LTS it was not necessary to update the following file. 
In Ubuntu 14.04.3 LTS is was necessary to do the following:  
```
sudo nano /etc/apparmor.d/usr.sbin.mysqld
```
add lines:  
```  
/home/naifo/cronjobs/smart_building_server_update/data_to_pi_access/ r,
/home/naifo/cronjobs/smart_building_server_update/data_to_pi_access/* rw,
```

then:   
```  
sudo /etc/init.d/apparmor reload  
```
In another case (`Ubuntu 14.04.5 LTS` and `mysql 5.5.53`) it was necessary to add to `/etc/mysql/my.cnf` the following line on the section `[mysqld]`:   
```
secure_file_priv=""
```


Then, `sudo python server_to_csv.py` should run successfully.   

**csv_to_db.py:**  
`data_pi` directory and inner files are configured with ownership naifo:naifo (Or the user which connects via rsync to put the files in it).  
This script needs pandas and mysqldb. At the same time pandas depends on numpy.   
So, the following installs are necessary:  
```
sudo pip install numpy==1.10.4
sudo pip install pandas==0.17.1
sudo apt-get install python-mysqldb
```  
Make sure the right versions are installed with:  
```
import numpy
import pandas
numpy.__version__
pandas.__version__
```
If something fails, make sure that `numpy` and `pandas` are not installed by `apt-get`. For this do:  
```
import pandas
print(pandas)
```
and  
```
import numpy
print(numpy)
```
and then delete the package in the directory indicated in the outputs. Also delete the egg files or directories.  

Then, `sudo python csv_to_db.py` should run successfully.  


**populate_attendance.py:**  

This script depends on sqlalchemy, so it should be installed with:  
```
sudo pip install sqlalchemy
```

Then, 'sudo python populate_attendance.py` should run successfully.  

