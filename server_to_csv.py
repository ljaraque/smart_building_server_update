# this file run based on a cronjob to be included in /etc/crontab with the following line:
# */5 * * * * root /usr/bin/python /home/naifo/code/mysql/server_to_csv.py
# Note: This file must be executed as sudo

import time
import os

path = '/home/naifo/cronjobs/smart_building_server_update/'

start=time.time()
os.system("sudo rm " + path + "data_server/*")
#export complete Employees table to CSV
#os.system("mysql -uroot -powen < /home/naifo/code/mysql/server_employees_export.sql")
#this is the export table to dump version
os.system("mysql -uroot -powen < " + path + "server_employees_export_dump.sql")
os.system("mysqldump -uroot -powen alfamasdb employees_dump > " + path +"data_server/server_employees_dump.sql")

#export complete Residents table to CSV
os.system("mv " + path + "data_to_pi_access/server_residents.csv " + path + "data_to_pi_access/server_residents.csv_old")
os.system("mysql -uroot -powen < " + path + "server_residents_export_csv.sql")
#this is the export table to dump version
os.system("mysql -uroot -powen < " + path + "server_residents_export_dump.sql")
os.system("mysqldump -uroot -powen alfamasdb residents_dump > " + path + "data_server/server_residents_dump.sql")

end=time.time()

print "elapsed time: "+ str(end - start)

