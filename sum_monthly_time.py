# coding=utf-8
"""
Title		: Sum of Monthly Accumulated Times by Each Employee
By		: Alfanetics
Date		: 2015.12.14
Description	: 
"""

# IMPORTS & GENERAL OPTIONS
from pandas import DataFrame, to_datetime, set_option, option_context,concat
from sqlalchemy import create_engine
from datetime import datetime, timedelta
import sys

set_option('display.width', 1000) # set_option is from pandas display
date_format = "%Y-%m-%d %H:%M:%S"

try:
    #GET DATA FROM DB TO DATAFRAME
    #create db engine with SqlAlchemy
    engine=create_engine("mysql://root:owen@localhost/alfamasdb")
    #opens connection
    connection = engine.connect()

    #get whole attendancelog of current month
    #Check what happen if query is empty!!!!!!!!!!!!!!!!!!!
    today = datetime.today()
    first = today.replace(day=1)
    last_day_of_previous_month = first - timedelta(seconds=1)
    last_month=last_day_of_previous_month.strftime("%Y-%m-%d")
except Exception as e: 
    print e
    sys.exit()

try:
    last_month_sql="select * from attendancelogs where datetime >"+"\""+str(last_month)+"\""
    last_month_query=connection.execute(last_month_sql)
    df_last_month=DataFrame(last_month_query.fetchall())
    df_last_month.columns=last_month_query.keys()
    #print df_last_month
    #sys.exit()
except Exception as e: 
    print e
    sys.exit()


def cum_time(df_last_month):
    # get users active in this month
    active_employees = df_last_month['rut'].unique()
    print active_employees

    # iterate over different employees
    df_last_month_result=df_last_month
    for employee in active_employees:
        print "EMPLOYEE: " +str(employee) + "------------------------------------------"
        mask_this_employee=(df_last_month.rut==employee)
        #print "MASK: " + str(mask_this_employee)
        indices_this_employee = mask_this_employee[mask_this_employee==True].index.tolist()
        print "INDICES: " + str(indices_this_employee)
        df_this_employee = df_last_month.iloc[mask_this_employee.values,:] # this can also be df_this_employee = df_last_month.loc[indices_this_employee,:]
        df_this_employee.departuretime = df_this_employee.departuretime.astype(float)
        print df_this_employee
        df_this_employee['totalhours']=df_this_employee.departuretime.cumsum()
        print df_this_employee
        df_last_month_result.loc[indices_this_employee,'totalhours']= df_this_employee.loc[indices_this_employee,'totalhours']# loc does indexing by index, iloc does it by position
    return df_last_month_result

try:
    print df_last_month
    df_last_month_result=cum_time(df_last_month)

    print df_last_month_result

    first_sql_id=df_last_month_result.id[0]
    print first_sql_id

    last_sql_id=df_last_month_result[-1:]['id'].tolist()[0]
    print last_sql_id

    x=df_last_month_result[df_last_month_result.id==last_sql_id].totalhours.tolist()[0]
    print x
    for n in range(first_sql_id,last_sql_id+1):
        total_hours_n = df_last_month_result[df_last_month_result.id==n].totalhours.tolist()[0]
        sql="update attendancelogs set totalhours = "+ str(total_hours_n) + " where id = "+str(n)
        last_month_query=connection.execute(sql)
    #close connection
    connection.close()

except Exception as e: 
    print e
    sys.exit()







#PENDIENTE:
#*general:
#- accion si este mes esta vacio (RESUELTO CON TRY. SI UN QUERY NULO OCASIONA PROBLEMA DEBERÍA ABORTAR EL CODIGO)
#- calculo final de cierre de mes anterior cada vez que today() es 1 de nuevo mes o si no se hizo aun siendo otro dia del nuevo mes (NADA HECHO, CREO QUE 
#NO SE NECESITA HACER NADA PUES HASTA ULTIMOS 5 MINUTOS DE ULTIMO DIA DEL MES SE HABRAN RECALCULADO LOS TIEMPOS ACUMULATIVOS)

