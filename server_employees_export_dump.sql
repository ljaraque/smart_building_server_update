# Mysql statements to send table to dump file

connect alfamasdb;

drop table if exists employees_dump;

create table employees_dump
as
select id as id
, fingerprint_id as fingerprint_id
, rut as rut
, name as name
, lastname as lastname
, created as created
, modified as modified
, enrolstate as enrolstate
from users
where mode="trabajador"
;


