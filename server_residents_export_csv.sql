# Mysql statements to query, merge fields and output to csv file

connect alfamasdb;

(select 'id','cardnumber','active','privileges')

union

(select id as id
, cardnumber as cardnumber
, active as active
, concat_ws('-',p1,p2,p3,p4,p5,p6,p7,p8,p9,p10) as privileges
from cards
into outfile '/home/naifo/cronjobs/smart_building_server_update/data_to_pi_access/server_residents.csv'
fields terminated by ','
enclosed by '' # sometimes use " inside for text
lines terminated by '\n');
