"""
Title		: Attendance Hours Calculation
By		: Alfanetics
Date		: 2015.09.5
Description	: 
"""

# IMPORTS
from pandas import DataFrame, to_datetime, set_option, option_context,concat
from sqlalchemy import create_engine
from datetime import datetime

path = '/home/naifo/cronjobs/smart_building_server_update/'

set_option('display.width', 1000) # set_option is from pandas

date_format = "%Y-%m-%d %H:%M:%S"

# GET NEW DATA FROM EVENTLOGS

# recover events pending to close from last time
df_previous_events=DataFrame.from_csv(path + 'remaining_events_i-1.csv')
df_previous_events['name']=None
df_previous_events['lastname']=None
# get last id
last_previous_id_of_events = df_previous_events.iloc[-1,0]
#last_previous_id_of_events = 1050

# creates engine
engine=create_engine("mysql://root:owen@localhost/alfamasdb")

# opens connection
connection = engine.connect()

# query from stored date and on
try:
    last_date_sql="select datetime from eventlogs where id>"+str(last_previous_id_of_events) #'X'" # it is assumed that last round marked with an X in name column the last row
    last_date_query=connection.execute(last_date_sql)
    df_last_date=DataFrame(last_date_query.fetchall())
    df_last_date.columns=last_date_query.keys()
except: 
    exit()
print "Last element i-1: "+ df_last_date.iloc[0,0]

last_date=df_last_date.iloc[0,0] # way to get the raw text from df (without frame). The same as doing date='2015-08-23 13:26:24'
new_events_sql="select * from eventlogs where str_to_date(datetime, '%%Y-%%m-%%d %%H:%%i:%%S') >= str_to_date(\'" +str(last_date) + "\','%%Y-%%m-%%d %%H:%%i:%%S')"

# executes query
new_events_query=connection.execute(new_events_sql)

# query to pandas dataframe
df_new_events_temp = DataFrame(new_events_query.fetchall())
df_new_events_temp.columns=new_events_query.keys()

if df_new_events_temp.iloc[0,0]==df_previous_events.iloc[-1,0]+1:
    df_new_events=concat([df_previous_events, df_new_events_temp],ignore_index=True) # concat from pandas
else:
    df_new_events=df_new_events_temp

df_new_events_to_save = DataFrame(data=None, columns=df_new_events.columns) # this is to save the last registers to continue calculations nex time


# print query
print df_new_events


# query and dataframe of trabajadores
workers_sql="select * from users where mode='Trabajador'"
workers_query=connection.execute(workers_sql)
df_workers = DataFrame(workers_query.fetchall())
df_workers.columns=workers_query.keys()
print df_workers
print "\n"
#print df_workers.name[df_workers.rut=="12.657.592-0"].values[0]

# DATA HANDLING

# get users active in the new period
active_employees = df_new_events['rut'].unique()

#------------ debug_init
print "\n Users of the period:"
print active_employees
print "\n"
#------------ debug_end


# create output attendancelogs dataframe with specific order of columns
cols=['name', 'lastname', 'rut', 'entry_datetime', 'exit_datetime', 'event_duration', 'accumulated_month']
df_attendancelogs = DataFrame(columns=cols)

# iterate over different employees active in the period
for employee in active_employees:
    print "EMPLOYEE: " +str(employee) + "------------------------------------------"
    mask_this_employee=(df_new_events.rut==employee)
    df_this_employee = df_new_events.iloc[mask_this_employee.values,:] # 
    #print df_this_employee
    ix=0 # index cannot be used because it is the index of the big df and not the subset

    # iterate over log rows of current employee
    for index, row in df_this_employee.iterrows():
        #print row
        try:
            row_name=df_workers.name[df_workers.rut==row.rut].values[0]
        except:
            row_name="Nulo"
        try:
            row_lastname=df_workers.lastname[df_workers.rut==row.rut].values[0]
        except:
            row_lastname="Nulo"
        if row.event =="entrada":
            time_in = to_datetime(row.datetime)
            time_in_dt = datetime.strptime(str(time_in), date_format)
            if ix <= len(df_this_employee.index)-2:
                time_next = to_datetime(df_this_employee.iloc[ix+1,:].datetime)
                time_next_dt = datetime.strptime(str(time_next), date_format)
                time_dif = time_next_dt - time_in_dt
                if time_dif.total_seconds() <= 12*60*60:
                    if df_this_employee.iloc[ix+1,:].event=="entrada":
                       print "esta es entrada y siguiente es entrada dentro de 12 horas"
                       # in next line row.name is avoided and instead used row['name'] because row.name is reserved to mean id
                       df_attendancelogs = df_attendancelogs.append({'name': row_name, 'lastname':row_lastname, 'rut':row.rut, 'entry_datetime':row.datetime, 'exit_datetime':'0', 'event_duration':'0', 'accumulated_month':'---'}, ignore_index=True)
                    if df_this_employee.iloc[ix+1,:].event=="salida":
                       print "esta es entrada y siguiente es salida dentro de 12 horas"
                       df_attendancelogs = df_attendancelogs.append({'name': row_name, 'lastname':row_lastname, 'rut':row.rut, 'entry_datetime':str(time_in), 'exit_datetime':str(time_next), 'event_duration':str(round(time_dif.total_seconds()/3600,2)), 'accumulated_month':'---'}, ignore_index=True)
                elif time_dif.total_seconds() > 12*60*60:
                    if df_this_employee.iloc[ix+1,:].event=="entrada":
                       print "esta es entrada y siguiente es entrada pero despues de 12 horas"
                       df_attendancelogs = df_attendancelogs.append({'name': row_name, 'lastname':row_lastname, 'rut':row.rut, 'entry_datetime':row.datetime, 'exit_datetime':'0', 'event_duration':'0', 'accumulated_month':'---'}, ignore_index=True)
                    if df_this_employee.iloc[ix+1,:].event=="salida":
                       print "esta es entrada y siguiente es salida pero despues de 12 horas"
                       #df_attendancelogs = df_attendancelogs.append({'name':row['name'], 'lastname':row.lastname, 'rut':row.rut, 'entry_datetime':str(time_in), 'exit_datetime':str(time_next), 'event_duration':str(time_dif), 'accumulated_month':'pending'}, ignore_index=True)
                       df_attendancelogs = df_attendancelogs.append({'name': row_name, 'lastname':row_lastname, 'rut':row.rut, 'entry_datetime':row.datetime, 'exit_datetime':'0', 'event_duration':'0', 'accumulated_month':'---'}, ignore_index=True)
            elif ix > len(df_this_employee.index)-2:
                print "esta es entrada y no hay siguiente evento"
                time_max_all_employees = to_datetime(df_new_events.iloc[-1,:].datetime)
                time_max_all_employees_dt = datetime.strptime(str(time_max_all_employees), date_format)
                time_dif_with_max = time_max_all_employees_dt - time_in_dt
                print "tiempo evento maximo: "+str(time_max_all_employees_dt)
                print "diferencia con evento maximo: "+str(  time_dif_with_max.total_seconds()/(60*60)   ) + " horas."
                # si esta es entrada y no hay siguiente evento de este employee y ademas la hora del ultimo evento de todos los usuarios esta a lo mas a 12 horas de distancia temporal
                if time_dif_with_max.total_seconds() <= 12*60*60:
                    print "Sera almacenado"
                    df_new_events_to_save = df_new_events_to_save.append({'name': row_name, 'lastname':row_lastname, 'rut':row.rut, 'event':row.event, 'datetime':row.datetime}, ignore_index=True)
                # si este es entrada y ho hay siguiente evento de este employee y ademas la hora del ultimo evento de todos los usuarios esta a mas de 12 horas de distancia temporal
                if time_dif_with_max.total_seconds() > 12*60*60:
                    print "Sera terminado"
                    df_attendancelogs = df_attendancelogs.append({'name': row_name, 'lastname':row_lastname, 'rut':row.rut, 'entry_datetime':row.datetime, 'exit_datetime':'0', 'event_duration':'0', 'accumulated_month':'---'}, ignore_index=True)

            #------------ debug_init
            print "largo df: "+ str(len(df_this_employee.index))
            print "current index: " + str(ix)
            print "next index: "+ str(ix+1)
            print "employee: " +str(row.rut)
            print "este evento: " +str(time_in)
            if ix <= len(df_this_employee.index)-2:
                print "suguiente evento: " + str(df_this_employee.iloc[ix+1,:].datetime)
                print "time difference: " + str(time_next - time_in)
            print "\n"
            #------------ debug_end

        elif row.event=="salida":
            time_out = to_datetime(row.datetime)
            time_out_dt = datetime.strptime(str(time_out), date_format)
            time_max_all_employees = to_datetime(df_new_events.iloc[-1,:].datetime)
            time_max_all_employees_dt = datetime.strptime(str(time_max_all_employees), date_format)
            time_dif_with_max = time_max_all_employees_dt - time_out_dt

            if ix!=0:
                if df_this_employee.iloc[ix-1,:].event=="salida":
                    if time_dif_with_max.total_seconds() <= 12*60*60 and df_this_employee.iloc[ix-1,:].event == "salida":
                        print "sera almacenado"
                        df_new_events_to_save = df_new_events_to_save.append({'name': row_name, 'lastname':row_lastname, 'rut':row.rut, 'event':row.event, 'datetime':row.datetime}, ignore_index=True)
                    # si este es salida y ho hay siguiente evento de este employee y ademas la hora del ultimo evento de todos los usuarios esta a mas de 12 horas de distancia temporal
                    if time_dif_with_max.total_seconds() > 12*60*60 and df_this_employee.iloc[ix-1,:].event == "salida":
                        print "sera terminado"
                        df_attendancelogs = df_attendancelogs.append({'name': row_name, 'lastname':row_lastname, 'rut':row.rut, 'entry_datetime':'0', 'exit_datetime':row.datetime, 'event_duration':'0', 'accumulated_month':'---'}, ignore_index=True)
            elif ix==0:
                    df_attendancelogs = df_attendancelogs.append({'name': row_name, 'lastname':row_lastname, 'rut':row.rut, 'entry_datetime':'0', 'exit_datetime':row.datetime, 'event_duration':'0', 'accumulated_month':'---'}, ignore_index=True)

        ix+=1



if df_new_events_to_save.empty:
    # insert dummy row to avoid empty csv
    df_new_events_to_save = df_new_events_to_save.append({'name':'dummy'}, ignore_index=True)

df_new_events_to_save.iloc[-1,0]=df_new_events.iloc[-1,0]
df_new_events_to_save.to_csv(path + 'remaining_events_i-1.csv',na_values="None")

df1=DataFrame.from_csv(path + 'remaining_events_i-1.csv')
df1['name']=None
df1['lastname']=None
print df1
print "\n"


# final summary for checking: eventlogs and resulted attendancelogs
with option_context('display.max_rows', 999,'display.max_columns', 999): # if delete delete option_context import from pandas
    print df_new_events
    print "\n"
    print df_attendancelogs


# change names to match existing mysql table "attendancelogs"
df_attendancelogs.columns = ['name','lastname','rut','datetime','entrytime','departuretime','totalhours']

with option_context('display.max_rows', 999,'display.max_columns', 999): # if delete delete option_context import from pandas
    print df_attendancelogs

#replace zeros with date without time
mask_zeros_datetime = df_attendancelogs.datetime=="0"
mask_zeros_entrytime = df_attendancelogs.entrytime=="0"

df_attendancelogs['datetime'][mask_zeros_datetime]=df_attendancelogs['entrytime'][mask_zeros_datetime].str[:10]+" - no marca"
df_attendancelogs['entrytime'][mask_zeros_entrytime]=df_attendancelogs['datetime'][mask_zeros_entrytime].str[:10]+" - no marca"


df_attendancelogs.to_sql('attendancelogs',engine,flavor='mysql', if_exists='append',index=False)


# add names and lastnames
# query similar to: select u.id as id, u.name as name, u.lastname as lastname from users as u inner join attendancelogs as al on u.rut = al.rut;
# ojo: problema-error si el rut no existe en users.

# close connection
connection.close()









'''
from datetime import datetime

date_format = "%Y-%m-%d %H:%M:%S"
#date_format = "%m/%d/%Y"

myDate1_i="2015-08-23 11:13:21"
mydate1_o="2015-08-23 18:15:20"

a1 = datetime.strptime(myDate1_i, date_format)
b1 = datetime.strptime(mydate1_o, date_format)

print a1.hour
print a1.minute
print a1.second

delta1 = b1 - a1
print delta1
print delta1.days
print delta1.total_seconds()
print delta1.seconds

myDate2_i="2015-08-23 11:13:21"
mydate2_o="2015-08-23 18:15:20"

a2 = datetime.strptime(myDate2_i, date_format)
b2 = datetime.strptime(mydate2_o, date_format)
delta2 = b2 - a2
print delta2

print delta1+delta2


'''










#  # query with variable last hours
#  #look_back_interval=900
#  #sql= "select * from eventlogs where str_to_date(datetime,'%%Y-%%m-%%d %%H:%%i:%%S') > (select max(str_to_date(datetime,'%%Y-%%m-%%d %%H:%%i:%%S')) from eventlogs)-interval "+str(look_back_interval)+" hour"


#  # executes query
#  query=connection.execute(sql)

#  # query to pandas dataframe
#  df=DataFrame(query.fetchall())
#  df.columns=query.keys()

#  # close connection
#  connection.close()

#  print df

#  # data handling
#  df['datetime']=to_datetime(df['datetime'])

#  print df.dtypes

#  print df.iloc[-2:]

#  print df.tail(2)['datetime']


#  date1= df.iloc[-23:-22]['datetime']
#  date2= df.iloc[-1:]['datetime']
#  print date1.values
#  print date2.values

#  print (date2.values - date1.values)/1000000000
