# -------------------------------------------------------------------------------------- 
#
# This code import csv file into mysql new table 'employeesPi'. Then update a previously 
# existing table 'users' with the data from this imported data according to the query
# specified in .sql file. 
# In summary this code: updates existing table 'users' from csv file 'employees.csv'. 
# project name: smart community
# author: @naifo
#
# --------------------------------------------------------------------------------------

# this file run based on a cronjob to be included in /etc/crontab with the following line:
# */5 * * * * root /usr/bin/python /home/naifo/code/mysql/csv_to_db.py
# Note: This file must be executed as sudo

# WARNING: Pandas version other than '0.16.2' may result in problems. Pandas for raspberry Pi 
# at the moment of this release (2015.08.21) cannot be higher than '0.8.0'. So in Rpi conversion to db is done
# by other means.

import pandas as pd
import os

from pandas.io import sql
import MySQLdb

path = '/home/naifo/cronjobs/smart_building_server_update/'

#creation of dataframes with data from CSVs coming from Pi
df_employees_pi= pd.read_csv(path + 'data_pi/employees.csv')
df_registers_pi= pd.read_csv(path + 'data_pi/registers.csv')
df_access_pi= pd.read_csv(path + 'data_pi/access.csv')

#connection to db and creation of tables from each dataframe
con = MySQLdb.connect(host="localhost",user="root",passwd="owen",db="alfamasdb")
sql.write_frame(df_employees_pi, con=con, name='employeesPi', if_exists='replace', flavor='mysql')
sql.write_frame(df_registers_pi, con=con, name='registersPi', if_exists='replace', flavor='mysql')
sql.write_frame(df_access_pi, con=con, name='accessPi', if_exists='replace', flavor='mysql')

con.close()

#agregar este query para updatear tabla target real
#update users, employeesPi set users.enrolstate = employeesPi.enrolstate where users.id = employeesPi.id;
os.system('sudo mysql -uroot -powen < ' + path + 'update_employees_to_users.sql')

#append new resident accesses 
os.system('sudo mysql -uroot -powen < ' + path + 'update_access_to_residentlogs.sql')

#append new employees events
os.system('sudo mysql -uroot -powen < ' + path + 'update_registers_to_eventlogs.sql')






